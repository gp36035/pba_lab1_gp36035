You should include a SECURITY.md file that highlights security related information for your project. This should contain:

__Disclosure policy.__
Define the procedure for what a reporter who finds a security issue needs to do in order to fully disclose the problem safely, including who to contact and how. Consider HackerOne’s community edition or simply a ‘security@’ email.

__Security Update policy.__
Define how you intend to update users about new security vulnerabilities as they are found.

__Security related configuration.__
Settings users should consider that would impact the security posture of deploying this project, such as HTTPS, authorization and many others.

__Known security gaps & future enhancements.__
Security improvements you haven’t gotten to yet. Inform users those security controls aren’t in place, and perhaps suggest they contribute an implementation! For some great reference examples of SECURITY.md files, look at Apache Storm and TensorFlow.


[źródło: www.snyk.io]
